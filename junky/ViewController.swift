//
//  ViewController.swift
//  junky
//
//  Created by Jin Hong on 2020-04-28.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

// MARK: - Action
struct ActionEvent: Event { let action: URL? }
class ActionViewController: UIViewController {
    private let eventBus: EventBus = EventBusSingleton.shared

    final func performAction(_ action: URL?) {
        eventBus.publish(ActionEvent(action: action))
    }

    func overridesAction(_ actionModel: ActionModel) -> Bool {
        return false
    }

    func handleAction(_ actionModel: ActionModel) {
        
    }
}

// MARK: - Splash
class SplashActionModel: ActionModel {
    func viewRepresentation() -> ActionViewController? { SplashViewController.create() }
}
class SplashViewController: ActionViewController {

    static func create() -> SplashViewController { SplashViewController() }

    private lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "SPLASH"
        return label
    }()

    private lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("BUTTON", for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()

    @objc private func buttonTapped(_ sender: UIButton) {
        // TODO: Better way of creating in-app action navigation
        performAction(URL(string: "easypark://app/enterPhone?showReferralImage=true"))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        view.addSubview(label)
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.addSubview(button)
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 100).isActive = true
    }
}

// MARK: - EnterPhone
class EnterPhoneActionModel: PreloginActionModel {
    let showReferralImage: Bool
    required init(action: URL) {
        // TODO: Better way to parse out properties from url
        let components = URLComponents(url: action, resolvingAgainstBaseURL: false)
        showReferralImage = components?.queryItems?
            .first { $0.name == "showReferralImage" }
            .map { $0.value == "true" } ?? false
    }
    func viewRepresentation() -> ActionViewController? { EnterPhoneViewController.create(showReferralImage: showReferralImage) }
}
class EnterPhoneViewController: ActionViewController {

    static func create(showReferralImage: Bool) -> EnterPhoneViewController { EnterPhoneViewController(showReferralImage: showReferralImage) }

    private(set) var showReferralImage: Bool

    private init(showReferralImage: Bool) {
        self.showReferralImage = showReferralImage
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "ENTER PHONE \(showReferralImage)"
        return label
    }()

    private lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("BUTTON", for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()

    @objc private func buttonTapped(_ sender: UIButton) {
        performAction(URL(string: "easypark://app/main?showReferralImage=true"))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .yellow
        view.addSubview(label)
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.addSubview(button)
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 100).isActive = true
    }

    override func handleAction(_ actionModel: ActionModel) {
        guard let model = actionModel as? EnterPhoneActionModel else { return }
        showReferralImage = model.showReferralImage
        label.text = "ENTER PHONE \(showReferralImage)"
    }
}

// MARK: - Main

struct MainViewPresentedEvent: Event {}
class MainActionModel: URLActionModel {
    let action: URL
    required init(action: URL) { self.action = action }
    func viewRepresentation() -> ActionViewController? { MainViewController.create() }
}
class MainViewController: ActionViewController {

    static func create() -> MainViewController { MainViewController()}

    private let eventBus: EventBus = EventBusSingleton.shared

    private lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "MAIN"
        return label
    }()

    private lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("BUTTON", for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()

    @objc private func buttonTapped(_ sender: UIButton) {
        performAction(URL(string: "easypark://app/addPayment?showReferralImage=true"))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
        view.addSubview(label)
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.addSubview(button)
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 100).isActive = true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        eventBus.publish(MainViewPresentedEvent())
    }
}

// MARK: - AddPayment
class AddPaymentActionModel: URLActionModel {
    let action: URL
    required init(action: URL) { self.action = action }
    func viewRepresentation() -> ActionViewController? { AddPaymentViewController.create() }
}
class AddPaymentViewController: ActionViewController {

    static func create() -> AddPaymentViewController { AddPaymentViewController() }

    private lazy var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "ADD PAYMENT"
        return label
    }()

    private lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("BUTTON", for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()

    @objc private func buttonTapped(_ sender: UIButton) {
        performAction(URL(string: "easypark://app/enterPhone?showReferralImage=true"))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        view.addSubview(label)
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.addSubview(button)
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 100).isActive = true
    }
}
