//
//  AppDelegate.swift
//  junky
//
//  Created by Jin Hong on 2020-04-28.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

protocol ActionInitializable { init(action: URL) }
protocol ActionModel { func viewRepresentation() -> ActionViewController? }
protocol URLActionModel: ActionInitializable, ActionModel {}
protocol PreloginActionModel: URLActionModel {}

extension URL {
    func toActionModel() -> ActionModel? {
        return moreScalableToActionModelQuestionMarkTwo()
//        switch path {
//        case "/main": return MainActionModel(action: self)
//        case "/enterPhone": return EnterPhoneActionModel(action: self)
//        default: print("log error toActionModel default"); return nil
//        }
    }

    func moreScalableToActionModelQuestionMarkTwo() -> ActionModel? {
        guard
            let bundleName = Bundle.main.infoDictionary?["CFBundleName"] as? String,
            let ActionModelClass = NSClassFromString("\(bundleName).\(lastPathComponent.toPascalCase)ActionModel") as? ActionInitializable.Type
            else { print("log error parsing action"); return nil }
        return ActionModelClass.init(action: self) as? ActionModel
    }
}

extension UIViewController {
    func topMostViewController() -> UIViewController? {
        if let navigationController = self as? UINavigationController {
            return navigationController.visibleViewController?.topMostViewController()
        }
        return presentedViewController.map { $0.topMostViewController() } ?? self
    }
}

extension String {
    /// camelCase to PascalCase
    var toPascalCase: String {
        return prefix(1).uppercased() + dropFirst()
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
}

