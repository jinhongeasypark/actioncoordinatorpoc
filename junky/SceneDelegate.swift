//
//  SceneDelegate.swift
//  junky
//
//  Created by Jin Hong on 2020-04-28.
//  Copyright © 2020 junk. All rights reserved.
//

import UIKit

class ConfigurationController {
    struct Configuration {
        let action: URL?
        static let defaultConfiguration = ConfigurationController.Configuration(action: nil)
    }
    func getConfiguration(completion: @escaping (Result<Configuration, Error>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3)) {
            completion(.success(Configuration(action: URL(string: "easypark://app/enterPhone"))))
        }
    }
}

class BranchService {
    struct Branch {
        let action: URL?
    }
    func getBranch(completion: @escaping (Result<Branch, Error>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(6)) {
            completion(.success(Branch(action: URL(string: "easypark://app/addPayment"))))
        }
    }
}

class Navigator: EventSubscriber {

    static let shared: Navigator = Navigator()
    private let eventBus: EventBus
    private var mainViewPresented: Bool
    private var deferredAction: ActionModel?
    private var sceneDelegate: SceneDelegate!

    private init(eventBus: EventBus = EventBusSingleton.shared) {
        self.eventBus = eventBus
        self.mainViewPresented = false
        self.deferredAction = nil
        self.subscribe()
    }

    private func subscribe() {
        eventBus.subscribe(self, to: ActionEvent.self, onEvent: onActionEvent)
        eventBus.subscribe(self, to: MainViewPresentedEvent.self, onEvent: onMainViewPresentedEvent)
    }

    deinit {
        eventBus.unsubscribe(self, from: ActionEvent.self)
        eventBus.unsubscribe(self, from: MainViewPresentedEvent.self)
    }

    private func onActionEvent(_ event: ActionEvent) {
        event.action?.toActionModel().map(performAction)
    }

    private func onMainViewPresentedEvent(_ event: MainViewPresentedEvent) {
        mainViewPresented = true
        deferredAction.map(performAction)
    }

    func configure(sceneDelegate: SceneDelegate) {
        self.sceneDelegate = sceneDelegate
    }

    func performAction(_ actionModel: ActionModel) {
        guard let viewToPresent = actionModel.viewRepresentation() else { print("log error performAction"); return }
        if sceneDelegate.window?.rootViewController == nil {
            // setRootViewController
            sceneDelegate.window?.rootViewController = viewToPresent
            sceneDelegate.window?.makeKeyAndVisible()
        } else if sceneDelegate.window?.rootViewController is SplashViewController && actionModel is MainActionModel {
            // replaceRootViewController
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) { [unowned self, viewToPresent] in
                self.sceneDelegate.window?.rootViewController = viewToPresent
                self.sceneDelegate.window?.rootViewController?.view.alpha = 0
                UIView.animate(withDuration: 0.25) {
                    self.sceneDelegate.window?.rootViewController?.view.alpha = 1
                }
            }
        } else {
            if mainViewPresented {
                let visibleViewController = sceneDelegate.window?.rootViewController?.topMostViewController()
                if let actionViewController = visibleViewController as? ActionViewController,
                    // overrideVisibleViewController
                    actionViewController.overridesAction(actionModel) {
                    actionViewController.handleAction(actionModel)
                } else {
                    // presentViewController
                    visibleViewController?.present(viewToPresent, animated: true, completion: nil)
                }
            } else {
                if actionModel is PreloginActionModel {
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) { [unowned self, viewToPresent] in
                        self.sceneDelegate.window?.rootViewController = viewToPresent
                        self.sceneDelegate.window?.rootViewController?.view.alpha = 0
                        UIView.animate(withDuration: 0.25) {
                            self.sceneDelegate.window?.rootViewController?.view.alpha = 1
                        }
                    }   
                } else {
                    deferredAction = actionModel is MainActionModel ? nil : actionModel
                }
            }
        }
    }
}

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    let navigator = Navigator.shared
    let configurationController = ConfigurationController()
    let branchService = BranchService()

    override init() {
        super.init()
        navigator.configure(sceneDelegate: self)
    }

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        window = (scene as? UIWindowScene).map(UIWindow.init)
        navigator.performAction(SplashActionModel())
        configurationController.getConfiguration { [unowned self] result in
            switch result {
            case .success(let configuration): self.handleConfiguration(configuration)
            case .failure(let error): print(error); self.handleConfiguration(.defaultConfiguration)
            }
        }
        branchService.getBranch { [weak self] result in
            switch result {
            case .success(let branch): self?.handleBranch(branch)
            case .failure(let error): print(error)
            }
        }
    }

    private func handleConfiguration(_ configuration: ConfigurationController.Configuration) {
        configuration.action?.toActionModel().map(navigator.performAction)
    }

    private func handleBranch(_ branch: BranchService.Branch) {
        branch.action?.toActionModel().map(navigator.performAction)
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}

